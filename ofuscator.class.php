<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author    Nathan Rodriguez nathan.rodram at gmail.com
 * 
 */

class Ofuscator {

    private static $exceptions = array('__construct', '__destroy', '__toString', '__call', '__callStatic', '__get', '__set', '__isset', '__unset', '__sleep', '__wakeup', '__invoke', '__invoke', '__set_state', '__clone', '_GET', '_POST', '_SERVER', '_REQUEST','_SESSION','GLOBALS','_FILES','_ENV','_COOKIE','argc','argv');

    public static function ofuscateClassNames($code) {
        $pattern = "/class ([a-zA-Z0-9_]+)/";
        return self::replaceMatches($code, $pattern);
    }

    public static function ofuscateFunctionNames($code) {
        $pattern = "/function ([a-zA-Z0-9_]+)/";
        return self::replaceMatches($code, $pattern);
    }

    public static function ofuscateVariableNames($code) {
        $pattern = '/\$([a-zA-Z0-9_]+)/';
        return self::replaceMatches($code, $pattern);
    }

    public static function ofuscateAll($code) {
        $code = self::ofuscateClassNames($code);
        $code = self::ofuscateFunctionNames($code);
        $code = self::ofuscateVariableNames($code);
        return $code;
    }

    private static function removeComments($code) {
        return preg_replace('#/\*[^*]*\*+([^/][^*]*\*+)*/#', '', $code);
    }

    private static function removeNewLines($code) {
        return preg_replace('/\s\s+/', ' ', $code);
    }

    private static function replaceMatches($code, $pattern) {

        $code = self::removeComments($code);
        $matchExist = preg_match_all($pattern, $code, $matches);
        if ($matchExist) {
            foreach ($matches[1] as $k => $v) {

                if (!in_array($v, self::$exceptions)) {
                    $row = str_replace($v, '_' . strtoupper(md5($v)), $matches[0][$k]);
                    $code = str_replace($matches[0][$k], $row, $code);
                }
            }
        }

        $code = self::removeNewLines($code);
        return $code;
    }

}

?>

