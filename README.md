# PHP 5 class to ofuscate php code
##License

PHP ofuscator is licensed under the GPL license version 3.

http://www.gnu.org/licenses/gpl-3.0.txt

##Contact

Please email me if you have suggestions for improvements
or bug fixes: <nathan.rodram at gmail.com>.
